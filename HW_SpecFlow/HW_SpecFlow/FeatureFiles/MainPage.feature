﻿Feature: MainPage
	
As a user, 
I want to be able to subscribe to the newsletter of information about promotions by e-mail 
In order to be able to make profitable purchases

Background: 
Given Allo page is opened

Scenario: Sending an email address to subscribe to the newsletter
	When User fill email field for subscribe 'trytosee_element_666@tratata.com'
	When User clicks on the subscribe button
	Then User see successful message about his subscribe

