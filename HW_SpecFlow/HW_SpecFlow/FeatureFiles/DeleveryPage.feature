﻿Feature: DeliveryPage
	As a user
	I want to go to the delivery page
	In order to read information about delivery

Background: 
	Given Allo page is opened

Scenario: Go to the delivery page
	When I click on the delivery button
	Then Delivery page is opened
	Then I see see information about delivery