﻿Feature: ContactsPage
As a user
  I want to see contact information
  In order to see phone number and call to contact center 

As a user 
  I want to be able to send a message to the store support 
  In order to receive feedback by phone call or e-mail



Background: Start page
	Given Allo page is opened

Scenario: Open contacts page as an unauthorized user
	When User clicks on Contacts Page
	Then User see thet Contact page is opened
	Then User see first Free Ukraine number
	Then User see second Free Ukraine number
	Then User see first call_us_City number
	Then User see second call_us_City number

Scenario: Open contacts page as an unauthorized user to submit a form with a message with email
	When User clicks on Contacts Page
	Then User see thet Contact page is opened
	When User fill user name field 'Stepan'
	When User fill email field 'stepant@tratata.com'
	When User choose the subject of the message - Other questions
	When User fill user ask message to feedback message field 'Some ask text'
	When User click to send button
	Then User see successful message

Scenario: Open contacts page as an unauthorized user to submit a form with a message with phone number
	When User clicks on Contacts Page
	Then User see thet Contact page is opened
	When User fill user name field 'Tolik'
	When User chose phone for feedback with me
	When User fill phone number field '380332221100'
	When User choose the subject of the message - Other questions
	When User fill user ask message to feedback message field 'Some ask text with phone number'
	When User click to send button
	Then User see successful message