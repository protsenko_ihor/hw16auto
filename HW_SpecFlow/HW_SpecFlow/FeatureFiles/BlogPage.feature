﻿Feature: BlogPage
	
	As a user
	I want to go to the blog page from main page  
	In order to receive up-to-date news and articles with product reviews

Background: 
	Given Allo page is opened

Scenario: The ability to go from the main page of the site to the blog page for an unauthorized user who has visited the site
	When User click on the blog button
	Then Blog page opened
	Then User sees the news tab on the blog page
	Then User sees the articles tab on the blog page
	Then User sees the video tab on the blog page