﻿Feature: CreditPage
	As a user
	I want to be able to familiarize myself with the conditions of consumer loans 
	In order to be able to make a purchase on credit

Background: 
	Given Allo page is opened

Scenario: Opening the credits page from the home page of the site
	When User click on the credit button
	Then Credit page is opened
	Then User see information about credits minimum conditions