﻿Feature: Cart
As a visitor
I want to see the cart on the home page
In order to to see that it is empty by default


As a user, 
I want to see the list of products of interest in the cart 
In order to see the list of products before placing an order

As a user,
I want to be able to remove an item from my cart
in order to be able to leave only the product for which I want to place an order

Background: Start page
	Given Allo page is opened

Scenario: Checking for the absence of items in the cart for a new visitor
    When User click cart button
    Then User see text like 'Ваша корзина пока пуста.'
    When User click close cart

#Scenario: Adding two items to the cart and checking their display for an unauthorized user
#    When User click cart button
#    Then User see text like 'Ваша корзина пока пуста.'
#    When User click close cart
#    When User add first product from the list of best sellers
#    When User add second product from the list of best sellers
#    When User click cart button
#    Then User see text wiht name of first product
#    Then User see text wiht name of second product
#    When User click close cart
#
#Scenario: Adding two items to the cart and deleting one of the added products
#    When User click cart button
#    Then User see text like 'Ваша корзина пока пуста.'
#    When User click close cart
#    When User add first product from the list of best sellers
#    When User add second product from the list of best sellers
#    When User click cart button
#    When User deleting first product
#    Then User see text wiht name of second product
#    When User click close cart