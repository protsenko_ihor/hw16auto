﻿Feature: ShopsPage
	As a user
	I want to go to the shops page
	In order to see addresses of the shops

Background: 
	Given Allo page is opened

Scenario: Go to the shops page
	When I click on the shops button
	Then Shops page is opened
	Then I see see addresses of the shops