﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class BlogPage
    {
        public IWebDriver _driver;

        public By getBlogPageTitle = By.XPath("/html/head/title");
        //Блог АЛЛО - Обзор мобильных устройств и Цифровой электроники
        
        public By getBlogNewsButton = By.XPath("/html/body/div[2]/header/div/div/div/div/div[2]/div[2]/nav/ul/li[1]/a");
        public By getBlogArticlesButton = By.XPath("/html/body/div[2]/header/div/div/div/div/div[2]/div[2]/nav/ul/li[2]/a");
        public By getBlogVideoButton = By.XPath("/html/body/div[2]/header/div/div/div/div/div[2]/div[2]/nav/ul/li[3]/a");
        //Новости
        //Статьи
        //Видео

        public BlogPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindBlogTitle()
        {
            return _driver.FindElement(getBlogPageTitle);
        }

        public string GexTextBlogTitle()
        {
            return FindBlogTitle().GetAttribute("text");
        }

        public string GetTextBlogNewsButton()
        {
            return _driver.FindElement(getBlogNewsButton).Text;
        }

        public string GetTextBlogArticlesButton()
        {
            return _driver.FindElement(getBlogArticlesButton).Text;
        }

        public string GetTextBlogVideoButton()
        {
            return _driver.FindElement(getBlogVideoButton).Text;
        }

    }
}
