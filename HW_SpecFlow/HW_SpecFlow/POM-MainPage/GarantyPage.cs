﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class GarantyPage
    {
        private IWebDriver _driver;

        public By getGarantyTitle = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[1]/h2");
        public By getGarantyInfo = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[1]/h3");

        public GarantyPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindGarantyTitle()
        {
            return _driver.FindElement(getGarantyTitle);
        }

        public string GexTextGarantyTitle()
        {
            return FindGarantyTitle().Text;
        }

        public IWebElement FindGarantyInfo()
        {
            return _driver.FindElement(getGarantyInfo);
        }

        public string GetTextGarantyInfo()
        {
            return FindGarantyInfo().Text;
        }
    }
}

