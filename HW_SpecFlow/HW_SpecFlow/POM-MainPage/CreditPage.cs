﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class CreditPage
    {
        private IWebDriver _driver;

        public By getCreditH2Title = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");
        public By getCreditInfoMinTask = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h4");
        //Минимальные требования для оформления кредита:
        
        public CreditPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindCreditTitle()
        {
            return _driver.FindElement(getCreditH2Title);
        }

        public string GexTextCreditTitle()
        {
            return FindCreditTitle().Text;
        }

        public IWebElement FindCreditInfo()
        {
            return _driver.FindElement(getCreditInfoMinTask);
        }

        public string GetTextCreditInfo()
        {
            return FindCreditInfo().Text;
        }
    }
}
