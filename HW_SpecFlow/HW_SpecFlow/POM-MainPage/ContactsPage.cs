﻿using HW_SpecFlow.POM___MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.POM_MainPage
{
    public class ContactsPage
    {
        private IWebDriver _driver;
        public By getContactsPageTitleH1 = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");
        public By getButtonGoOfflineStorePage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[4]/div/a");

        public By getFirstFreeUkraineNumberContactsPage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[1]/div/div[1]/span");
        public By getSecondFreeUkraineNumberContactsPage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[1]/div/div[2]/span");


        public By getFirst_City_NumberContactsPage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/div[1]/span");
        public By getSecond_City_NumberContactsPage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/div[2]/span");

        public By getUserNameField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[1]/div/div[1]/input");
        public By getTypelistEmailOrPhone = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[1]");
        public By getPhoneToBackContact = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[1]/ul/li[2]");
        public By getUserPhoneNumberField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/div[1]/input");
        public By getUserEmailField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/div[1]/input");
        public By getFeedbackMessageType = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[3]/div/span");
        public By getUserMessageSubjectOtherQuestionsType = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[3]/div/ul/li[9]");
        public By getUserFeedbackMessageField = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[4]/div/div[1]/textarea");
        public By getFeedbackMessageButton = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/button");

        public By getSuccessfulMessage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/div/p");
        
        
        public ContactsPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindTitleContactsPage()
        {
            return _driver.FindElement(getContactsPageTitleH1);
        }

        public string GetTextFromTitleContactsPage()
        {
            return FindTitleContactsPage().Text;
        }

        public string GetTextFromFirstFreeUkraineNumberContactsPage()
        {
            return _driver.FindElement(getFirstFreeUkraineNumberContactsPage).Text;
        }
        public string GetTextFromSecondFreeUkraineNumberContactsPage()
        {
            return _driver.FindElement(getSecondFreeUkraineNumberContactsPage).Text;
        }

        public string GetTextFromFirst_City_NumberContactsPage()
        {
            return _driver.FindElement(getFirst_City_NumberContactsPage).Text;
        }
        public string GetTextFromSecond_City_NumberContactsPage()
        {
            return _driver.FindElement(getSecond_City_NumberContactsPage).Text;
        }

        public OfflineStoresPage ClickOnOfflineStorePagePage()
        {
            _driver.FindElement(getButtonGoOfflineStorePage).Click();
            return new OfflineStoresPage(_driver);
        }

        public ContactsPage EnterTextToNameField(string name)
        {
            _driver.FindElement(getUserNameField).SendKeys(name);
            return this;
        }

        public ContactsPage EnterClickToTypeOfContact()
        {
            _driver.FindElement(getTypelistEmailOrPhone).Click();
            return this;
        }
        public ContactsPage EnterClickToPhoneField()
        {
            _driver.FindElement(getPhoneToBackContact).Click();
            return this;
        }
        public ContactsPage EnterTextToEmailField(string name)
        {
            _driver.FindElement(getUserEmailField).SendKeys(name);
            return this;
        }

        public ContactsPage EnterTextToPhoneNumberField(string name)
        {
            _driver.FindElement(getUserPhoneNumberField).SendKeys(name);
            return this;
        }

        public ContactsPage EnterClickToMessageSubjectOtherQuestionsField()
        {
            _driver.FindElement(getUserMessageSubjectOtherQuestionsType).Click();
            return this;
        }
        
        public ContactsPage EnterClickToMessageSubjectType()
        {
            _driver.FindElement(getFeedbackMessageType).Click();
            return this;
        }

        public ContactsPage EnterTextToUserFeedbackMessageField(string name)
        {
            _driver.FindElement(getUserFeedbackMessageField).SendKeys(name);
            return this;
        }

        public ContactsPage EnterClickToSendMessageButton()
        {
            _driver.FindElement(getFeedbackMessageButton).Click();
            return this;
        }

        public string GetTextgetSuccessfulMessage()
        {
            return _driver.FindElement(getSuccessfulMessage).Text;
        }
       
    }
}
