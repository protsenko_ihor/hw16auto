﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class CartPopUp
    {
        private IWebDriver _driver;
        

        public By CartOpenButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div");
        public By CartMessageThetIsEmpty = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By CartCloseButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");

        public CartPopUp(IWebDriver driver)
        {
            _driver = driver;
            
        }
        public CartPopUp ClickCartOpenButton()
        {
            _driver.FindElement(CartOpenButton).Click();
            return this;
        }

        public string GetTextCartMessageThetIsEmpty()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.TextToBePresentInElementLocated(CartMessageThetIsEmpty, "Ваша корзина пока пуста."));
            return _driver.FindElement(CartMessageThetIsEmpty).Text;
        }


        public CartPopUp ClickCartCloseButton()
        {
            
            _driver.FindElement(CartCloseButton).Click();
            return this;
        }

    }
}
