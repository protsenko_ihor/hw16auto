﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class DeleveryPage
    {
        private IWebDriver _driver;

        public By getDeleveryTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");
        public By getDeleveryInfo = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div[2]/div[3]/div/h3");

        public DeleveryPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindDeleveryTitle()
        {
            return _driver.FindElement(getDeleveryTitle);
        }

        public string GexTextDeleveryTitle()
        {
            return FindDeleveryTitle().Text;
        }

        public IWebElement FindDeleveryInfo()
        {
            return _driver.FindElement(getDeleveryInfo);
        }

        public string GetTextDeleveryInfo()
        {
            return FindDeleveryInfo().Text;
        }
    }
}
