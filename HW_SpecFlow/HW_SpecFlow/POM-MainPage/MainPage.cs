﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_SpecFlow.POM_MainPage
{
    public class MainPage
    {
        public IWebDriver driver;
        Actions actions;

        public By getMainPagePageTitle = By.XPath("/html/head/title/text()");

        public By ButtonBlog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By ButtonFishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By ButtonJobPositions = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By ButtonShops = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By ButtonDeliveryAndPay = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By ButtonCredit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By ButtonWarrantyAndRefund = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By ButtonContacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");

      
        public By mainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By linkLeaveSmart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a");
        public By linkAlloMoney = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a");
        public By linkAlloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a");
        public By linkAlloExchange = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a");
        public By linkCutInPrice = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By linkPromotions = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a/p");

        public By ContactPhoneNumbers = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a/p");

        public By inputSearch = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By buttonLogin = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div");
        public By buttonCart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");

        public By getEmailFieldForSubscribe = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/div/div/input");
        public By getSubscribeButton = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/button");

        public By getSuccessfulMessageThanks = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/h3[1]");
        public By getEmailWithLinkMessage = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/p");

        public By getGarantiButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By getDeliveryPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By getShopsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");

        public MainPage(IWebDriver driver)
        {
            this.driver = driver;
            actions = new Actions(driver);
        }

        public IWebElement FindTitleMainPage()
        {
            return driver.FindElement(getMainPagePageTitle);
        }

        public string GetTextFromTitleMainPage()
        {
            return FindTitleMainPage().GetAttribute("text");
        }

 
        public MainPage EnterTextToEmailSubscribeField(string name)
        {
            actions.MoveToElement(driver.FindElement(getEmailFieldForSubscribe)).Click().Build().Perform();
            driver.FindElement(getEmailFieldForSubscribe).SendKeys(name);
            return this;
        }

        public MainPage EnterClickToEmailSubscribe()
        {
            
            actions.MoveToElement(driver.FindElement(getSubscribeButton), 60, 20).Click().Build().Perform();
            //driver.FindElement(getSubscribeButton).Click();
            return this;
        }

        public string GetTextFromSuccessfulMessageThanks()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.TextToBePresentInElementLocated(getSuccessfulMessageThanks, "Спасибо за подписку!"));
            return driver.FindElement(getSuccessfulMessageThanks).Text;
        }

        public string GetTextFromEmailWithLinkMessage()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.TextToBePresentInElementLocated(getEmailWithLinkMessage, "Письмо с ссылкой для подтверждения подписки отослано на ваш адрес."));
            return driver.FindElement(getEmailWithLinkMessage).Text;
        }
        public ContactsPage ClickOnContactsPage()
        {
            driver.FindElement(ButtonContacts).Click();
            return new ContactsPage(driver);
        }


        public BlogPage ClickOnButtonBlog()
        {
            driver.FindElement(ButtonBlog).Click();
            return new BlogPage(driver);
        }

        public CreditPage ClickOnCreditPage()
        {
            driver.FindElement(ButtonCredit).Click();
            return new CreditPage(driver);
        }

        public GarantyPage ClickOnGarantyButton()
        {
            driver.FindElement(getGarantiButton).Click();
            return new GarantyPage(driver);
        }

        public DeleveryPage ClickOnDeleveryPage()
        {
            driver.FindElement(getDeliveryPage).Click();
            return new DeleveryPage(driver);
        }

        public ShopsPage ClickOnShopsPage()
        {
            driver.FindElement(getShopsButton).Click();
            return new ShopsPage(driver);
        }
    }
}