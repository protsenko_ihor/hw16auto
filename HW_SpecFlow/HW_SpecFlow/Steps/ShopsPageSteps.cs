﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class ShopsPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);
        public ShopsPage shopsPage = new ShopsPage(ContactsPageSteps.driver);

        [When(@"I click on the shops button")]
        public void WhenIClickOnTheShopsButton()
        {
            shopsPage = mainPage.ClickOnShopsPage();
        }
        
        [Then(@"Shops page is opened")]
        public void ThenShopsPageIsOpened()
        {
            string shopsTitle = shopsPage.GexTextShopsTitle();
            Assert.AreEqual("Адреса магазинов", shopsTitle);
        }
        
        [Then(@"I see see addresses of the shops")]
        public void ThenISeeSeeAddressesOfTheShops()
        {
            string shopsInfo = shopsPage.GetTextShopsInfo();
            Assert.AreEqual("Введите ваш город:", shopsInfo);
        }
    }
}
