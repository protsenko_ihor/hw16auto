﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class MainPageSteps
    {
        //public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);


        [When(@"User fill email field for subscribe '(.*)'")]
        public void WhenUserFillEmailFieldForSubscribe(string p0)
        {
            mainPage.EnterTextToEmailSubscribeField(p0);
            //ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on the subscribe button")]
        public void WhenUserClicksOnTheSubscribeButton()
        {
            mainPage.EnterClickToEmailSubscribe();
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User see successful message about his subscribe")]
        public void ThenUserSeeSuccessfulMessageAboutHisSubscribe()
        {
            string successfulMessageThanks = mainPage.GetTextFromSuccessfulMessageThanks();
            string emailWithLinkMessage = mainPage.GetTextFromEmailWithLinkMessage();

            Assert.AreEqual("Спасибо за подписку!", successfulMessageThanks);
            Assert.AreEqual("Письмо с ссылкой для подтверждения подписки отослано на ваш адрес.", emailWithLinkMessage);
            //ScenarioContext.Current.Pending();
        }
    }
}
