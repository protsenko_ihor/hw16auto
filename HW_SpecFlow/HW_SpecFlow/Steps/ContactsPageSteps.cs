﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public  class ContactsPageSteps
    {
        public static IWebDriver driver;
        public static MainPage mainPage;
        public static ContactsPage contactsPage;

        [Before]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"D:\\");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            mainPage = new MainPage(driver);
            //contactsPage = new ContactsPage(driver);
        }
        [After]
        public void KillDriver()
        {
            driver.Quit();
        }

        [Given(@"Allo page is opened")]
        public static void GivenAlloPageIsOpened()
        {
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            driver.Manage().Window.Maximize();
            
            //ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on Contacts Page")]
        public void WhenUserClicksOnContactsPage()
        {

            contactsPage = mainPage.ClickOnContactsPage();
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see thet Contact page is opened")]
        public void ThenISeeThetContactPageIsOpened()
        {

            string contactsTitleText = contactsPage.GetTextFromTitleContactsPage();
            Assert.AreEqual("Консультации и заказы по телефонам", contactsTitleText);
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see first Free Ukraine number")]
        public void ThenISeeFirstFreeUkraineNumber()
        {
            string contactsFirstFreeUkraineNumber = contactsPage.GetTextFromFirstFreeUkraineNumberContactsPage();
            Assert.AreEqual("(0-800) 300-100", contactsFirstFreeUkraineNumber);
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see second Free Ukraine number")]
        public void ThenISeeSecondFreeUkraineNumber()
        {
            string contactsSecondFreeUkraineNumber = contactsPage.GetTextFromSecondFreeUkraineNumberContactsPage();
            Assert.AreEqual("(0-800) 200-100", contactsSecondFreeUkraineNumber);
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see first call_us_City number")]
        public void ThenISeeFirstCall_Us_CityNumber()
        {
            string contactsSecondFreeUkraineNumber = contactsPage.GetTextFromFirst_City_NumberContactsPage();
            Assert.AreEqual("(056) 790-12-34", contactsSecondFreeUkraineNumber);
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see second call_us_City number")]
        public void ThenISeeSecondCall_Us_CityNumber()
        {
            string contactsSecondFreeUkraineNumber = contactsPage.GetTextFromSecond_City_NumberContactsPage();
            Assert.AreEqual("(056) 797-63-23", contactsSecondFreeUkraineNumber);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User fill user name field '(.*)'")]
        public void WhenIFillUserNameField(string p0)
        {
            contactsPage.EnterTextToNameField(p0);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User fill email field '(.*)'")]
        public void WhenIFillEmailField(string p0)
        {
            contactsPage.EnterTextToEmailField(p0);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User choose the subject of the message - Other questions")]
        public void WhenIChooseTheSubjectOfTheMessage_OtherQuestions()
        {
            contactsPage.EnterClickToMessageSubjectType()
                    .EnterClickToMessageSubjectOtherQuestionsField();
            //ScenarioContext.Current.Pending();
        }

        [When(@"User fill user ask message to feedback message field '(.*)'")]
        public void WhenIFillUserAskMessageToFeedbackMessageField(string p0)
        {
            contactsPage.EnterTextToUserFeedbackMessageField(p0);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User click to send button")]
        public void WhenIClickToSendButton()
        {
            contactsPage.EnterClickToSendMessageButton();
            //ScenarioContext.Current.Pending();
        }

        [Then(@"User see successful message")]
        public void ThenISeeSuccessfulMessage()
        {
            string successfulMessage = contactsPage.GetTextgetSuccessfulMessage();
            Assert.AreEqual("Ваше сообщение отправлено в обработку.", successfulMessage);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User fill phone number field '(.*)'")]
        public void WhenIFillPhoneNumberField(string p0)
        {
            contactsPage.EnterTextToPhoneNumberField(p0);
            //ScenarioContext.Current.Pending();
        }

        [When(@"User chose phone for feedback with me")]
        public void WhenIChosePhoneForFeedbackWithMe()
        {
            contactsPage.EnterClickToTypeOfContact().EnterClickToPhoneField();
            //ScenarioContext.Current.Pending();
        }


    }
}
