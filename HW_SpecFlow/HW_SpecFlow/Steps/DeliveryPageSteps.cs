﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class DeliveryPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);
        public DeleveryPage deleveryPage = new DeleveryPage(ContactsPageSteps.driver);

        [When(@"I click on the delivery button")]
        public void WhenIClickOnTheDeliveryButton()
        {
            deleveryPage = mainPage.ClickOnDeleveryPage();
        }
        
        [Then(@"Delivery page is opened")]
        public void ThenDeliveryPageIsOpened()
        {
            string deliveryTitle = deleveryPage.GexTextDeleveryTitle();
            Assert.AreEqual("Доставка и оплата", deliveryTitle);
        }
        
        [Then(@"I see see information about delivery")]
        public void ThenISeeSeeInformationAboutDelivery()
        {
            string deliveryInfo = deleveryPage.GetTextDeleveryInfo();
            Assert.AreEqual("Как оформить заказ?", deliveryInfo);
        }
    }
}
