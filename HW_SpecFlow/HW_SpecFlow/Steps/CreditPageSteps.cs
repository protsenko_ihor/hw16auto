﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class CreditPageSteps
    {
        //public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);
        public CreditPage creditPage = new CreditPage(ContactsPageSteps.driver);


        [When(@"User click on the credit button")]
        public void WhenUserClickOnTheCreditButton()
        {
            creditPage = mainPage.ClickOnCreditPage();
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"Credit page is opened")]
        public void ThenCreditPageIsOpened()
        {
            string craditPageText = creditPage.GexTextCreditTitle();
            Assert.AreEqual("Кредит", craditPageText);
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User see information about credits minimum conditions")]
        public void ThenUserSeeInformationAboutCreditsMinimumConditions()
        {
            string craditPageTextMinCondition = creditPage.GetTextCreditInfo();
            Assert.AreEqual("Детальная информация про условия кредитования:", craditPageTextMinCondition);
            //ScenarioContext.Current.Pending();
        }
    }
}
