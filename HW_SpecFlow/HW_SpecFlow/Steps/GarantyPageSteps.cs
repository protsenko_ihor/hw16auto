﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class GarantyPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);
        public GarantyPage garantyPage = new GarantyPage(ContactsPageSteps.driver);

               
        [When(@"I click on the guarantee button")]
        public void WhenIClickOnTheGuaranteeButton()
        {
            garantyPage = mainPage.ClickOnGarantyButton();
        }
        
        [Then(@"Guarantee page is opened")]
        public void ThenGuaranteePageIsOpened()
        {
            string garantyTitle = garantyPage.GexTextGarantyTitle();
            Assert.AreEqual("ГАРАНТИЙНОЕ ОБСЛУЖИВАНИЕ", garantyTitle);
        }
        
        [Then(@"I see information about guarantee conditions")]
        public void ThenISeeInformationAboutGuaranteeConditions()
        {
            string garantyInfo = garantyPage.GetTextGarantyInfo();
            Assert.AreEqual("1. На все товары, приобретенные в АЛЛО, предоставляется гарантия, дающая право на:", garantyInfo);
        }
    }
}
