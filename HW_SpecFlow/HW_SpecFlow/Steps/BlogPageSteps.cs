﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class BlogPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage = new MainPage(ContactsPageSteps.driver);
        public BlogPage blogPage = new BlogPage(ContactsPageSteps.driver);



        [When(@"User click on the blog button")]
        public void WhenUserClickOnTheBlogButton()
        {
            
            blogPage = mainPage.ClickOnButtonBlog();
            
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"Blog page opened")]
        public void ThenBlogPageOpened()
        {
            
            blogPage._driver.SwitchTo().Window(ContactsPageSteps.driver.WindowHandles.Last());
            string blogPageTitle = blogPage.GexTextBlogTitle();
            Assert.AreEqual("Блог АЛЛО - Обзор мобильных устройств и Цифровой электроники", blogPageTitle);
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User sees the news tab on the blog page")]
        public void ThenUserSeesTheNewsTabOnTheBlogPage()
        {
            string nameTheNewsTab = blogPage.GetTextBlogNewsButton();
            Assert.AreEqual("НОВОСТИ", nameTheNewsTab);
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User sees the articles tab on the blog page")]
        public void ThenUserSeesTheArticlesTabOnTheBlogPage()
        {
            string nameTheArticlesTab = blogPage.GetTextBlogArticlesButton();
            Assert.AreEqual("СТАТЬИ", nameTheArticlesTab);
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User sees the video tab on the blog page")]
        public void ThenUserSeesTheVideoTabOnTheBlogPage()
        {
            string nameTheVideoTab = blogPage.GetTextBlogVideoButton();
            Assert.AreEqual("ВИДЕО", nameTheVideoTab);
            //ScenarioContext.Current.Pending();
        }
    }
}
