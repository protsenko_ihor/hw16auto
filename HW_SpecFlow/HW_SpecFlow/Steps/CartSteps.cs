﻿using HW_SpecFlow.POM_MainPage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HW_SpecFlow.Steps
{
    [Binding]
    public class CartSteps
    {
        public static CartPopUp cartPopUp = new CartPopUp(ContactsPageSteps.driver);

        [When(@"User click cart button")]
        public void WhenUserClickCartButton()
        {
            cartPopUp.ClickCartOpenButton();
            //ScenarioContext.Current.Pending();
        }
        
        [When(@"User click close cart")]
        public void WhenUserClickCloseCart()
        {
            cartPopUp.ClickCartCloseButton();
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"User see text like '(.*)'")]
        public void ThenUserSeeTextLike(string p0)
        {
            string messageThetIsEmpty = cartPopUp.GetTextCartMessageThetIsEmpty();
            Assert.AreEqual(p0, messageThetIsEmpty);
            //ScenarioContext.Current.Pending();
        }
    }
}
